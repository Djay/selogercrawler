package main

/**
 *  Save the ad into Redis DB
 *
 **/

import (
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

var client *redis.Client

// func main() {
//     initRedisClient()
//     ReadFolderFiles()
//     //storeKV("test", "value")
// }

// SaveFetchExec saves the current timestamp of the current fetch execution
func SaveFetchExec(sha1UrlAsked string) {
	now := time.Now()
	nanos := now.UnixNano()
	storeKV(sha1UrlAsked, strconv.FormatInt(nanos, 10))
}

// SaveAd add a new ad in the DB and return false if already exist
func SaveAd(sha1UrlAsked string, adToSave ad) (saved bool, err error) {
	initRedisClient()
	tmpKey := sha1UrlAsked + "/" + adToSave.Id
	if !isKeyAlreadySaved(tmpKey) {
		tmpVal := JSonOf(adToSave)
		fmt.Printf("\n== JSon value of the ad:\n\t%s\n", tmpVal)
		storeKV(tmpKey, tmpVal)
		return true, nil
	}
	return false, err
}

func initRedisClient() {
	// Get credentials from config file
	addr, pwd := GetRedisCredsFromConf()
	if len(addr) > 0 {
		client = redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: pwd,
			DB:       0, // use default DB
		})

		_, err := client.Ping().Result()
		Check(err)
		// fmt.Println(pong, err)
		// Output: PONG <nil>
	}
}

func storeKV(key string, val string) {
	initRedisClient()
	err := client.Set(key, val, 0).Err()
	if err != nil {
		panic(err)
	}
}

func isKeyAlreadySaved(key string) bool {
	initRedisClient()
	_, err := client.Get(key).Result()
	if err == redis.Nil {
		// key does not exist in the DB
		return false
	} else if err != nil {
		panic(err)
	} else {
		return true
	}
}
