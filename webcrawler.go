package main

/**
* Usage:
**/

import (
	"crypto/sha1"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"
)

type ad struct {
	Id      string
	Price   string
	Href    string
	Title   string
	ImgLink string
}

var (
	credPathArg   = flag.String("credPath", "./", "Path to the creds.conf")
	urlArg        = flag.String("url", "https://www.google.com", "URL to fetch")
	emailArg      = flag.String("email", "gerald.colin@goovy.com", "Email to send alerts")
	urlAsked      string
	EmailForAlert string
	CredFilePath  string
	sha1UrlAsked  string // hex of the sha1 of the Url asked to fetch
)

func main() {
	// url for test:
	// http://www.seloger.com/list.htm?idtt=2&naturebien=1,2,4&idtypebien=1&ci=750120&tri=d_dt_crea&surfacemin=50&surfacemax=55

	flag.Parse()

	// Retrieve the URL from the program args
	u, err := url.Parse(*urlArg)
	if err != nil {
		log.Fatal(err)
	}

	// Retrieve the path to the creds.conf file from the program args
	CredFilePath = *credPathArg

	// Retrieve the email from the program args
	EmailForAlert = *emailArg

	fmt.Printf("\nFetching at %s: %s\n", time.Now(), u.String())

	urlAsked = u.String()
	// Compute the sha1 of the url asked
	h := sha1.New()
	h.Write([]byte(urlAsked))
	bs := h.Sum(nil)
	sha1UrlAsked = fmt.Sprintf("%x", bs)
	fmt.Printf("\nsha1 of the url asked: %s\n", sha1UrlAsked)
	fecthURL(u.String())
}

func fecthURL(url string) {
	req, err := http.NewRequest("GET", url, nil)
	Check(err)

	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)")
	req.Header.Set("Accept-Language", "fr-FR;q=0.9")

	clt := &http.Client{
		Timeout: time.Second * 10,
	}
	resp, err := clt.Do(req)
	Check(err)

	defer resp.Body.Close()

	// Save the query to redis (for the audit)

	// body, err := ioutil.ReadAll(resp.Body)
	// check(err)
	// fmt.Printf("BODY\n:%s", string(body))
	ParseBodySeLoger(*resp)
}

// Check the error
func Check(e error) {
	if e != nil {
		panic(e)
	}
}

// JSonOf converts the ad struct to JSon
func JSonOf(adToConvert ad) string {
	tmpVal, err := json.Marshal(adToConvert)
	Check(err)
	return fmt.Sprintf("%s", tmpVal)
}
