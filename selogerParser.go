package main

import (
	"fmt"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

var (
	adToSave ad
)

// ParseBodySeLoger parse the HTTP response of seloger list of ads
func ParseBodySeLoger(resp http.Response) map[int]string {
	z := html.NewTokenizer(resp.Body)
	i := -1
	res := make(map[int]string)
	for {
		tt := z.Next()
		switch tt {
		case html.ErrorToken:
			return res
		case html.StartTagToken:
			t := z.Token()

			isDiv := t.Data == "div"
			if isDiv {
				ok, annonceId := getAnnonceId(t)
				if ok {
					adToSave.Id = annonceId
					// fmt.Printf("Annonce: %s\n", annonceId)
					i++
					res[i] = annonceId
				} else {
					ok, imgLink := getAdImg(z)
					if ok {
						adToSave.ImgLink = imgLink
					}
				}
			}

			isSpan := t.Data == "span"
			if isSpan {
				ok, price := getAnnoncePrice(t)
				if ok {
					z.Next()
					p := z.Token()
					price = p.Data
					adToSave.Price = strings.TrimSpace(price)
					// fmt.Printf("\t** %s **\n", price)
					fmt.Printf("------------------------------------- End of Ad --\n")
					isSaved, err := SaveAd(sha1UrlAsked, adToSave)
					Check(err)
					if isSaved {
						fmt.Printf("------------------------------------- Ad saved - Should notify user --\n")
						AlertUser([]string{"EMAIL"}, adToSave)
					}
				}
			}

			isAnchor := t.Data == "a"
			if isAnchor {
				ok, href, title := getAnnonceLink(t)
				if ok {
					adToSave.Title = title
					adToSave.Href = href
					// fmt.Printf("\t%s\n\t%s\n", title, href)
				} else {
					ok, href := getNextPageLink(t)
					if ok {
						fmt.Printf("\tFound next page to visit: %s\n", href)
						//fecthURL("http:" + strings.Replace(href, " ", "%20", -1))
					}
				}
			}
		}
	}
}

func getAnnonceId(t html.Token) (ok bool, res string) {
	for _, att := range t.Attr {
		if att.Key == "id" {
			idVal := att.Val
			if strings.Contains(idVal, "annonce") {
				res = idVal
				ok = true
			}

		}
	}
	return
}

func getAdImg(t html.Token) (ok bool, res string) {
	for _, att := range t.Attr {
		if att.Key == "class" && att.Val == "slideContent" {
			ok = true
		}
	}
	if ok {
		content := t.Data
		fmt.Printf("Found img div with content: %s", content)
		if i := strings.Index(content, "src="); i > 1 {
			res := strings.TrimRight(content[:i], ".jpg\"")
			res = res + ".jpg"
			fmt.Printf("\nImg link: %s", res)
		}
	}
	return
}

func getAnnonceLink(t html.Token) (ok bool, href string, title string) {
	for _, att := range t.Attr {
		if att.Key == "class" && att.Val == "c-pa-link" {
			ok = true
		}
	}
	if ok {
		for _, att := range t.Attr {
			if strings.ToLower(att.Key) == "title" {
				title = att.Val
			}
			if strings.ToLower(att.Key) == "href" {
				href = att.Val
			}
		}
	}
	return
}

func getAnnoncePrice(t html.Token) (ok bool, price string) {
	for _, att := range t.Attr {
		if att.Key == "class" && att.Val == "c-pa-cprice" {
			ok = true
			price = strings.TrimSpace(strings.Trim(t.String(), " \t\n"))
		}
	}
	return
}

func getNextPageLink(t html.Token) (ok bool, href string) {
	for _, att := range t.Attr {
		if att.Key == "class" && att.Val == "pagination-next" {
			ok = true
		}
	}
	if ok {
		for _, att := range t.Attr {
			if strings.ToLower(att.Key) == "href" {
				href = att.Val
			}
		}
	}
	return
}
