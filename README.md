# README #

This project get the real estate ads from french website seloger in order to have our own alerts.

###  How to use it ###

You need to have [Golang](https://golang.org/doc/install) installed and configured.
Just build `$ go build selogercrawler.go` and execute it with the url as argument: 
```
s./webrcrawler --url="http://www.seloger.com/list.htm?idtt=2&naturebien=1,2,4&idtypebien=1&ci=750120&tri=d_dt_crea&surfacemin=50&surfacemax=55" --email="someone@example.com" --credPath="/path/to/crawler/"
```


### Documentation ###

#### Redis DB ####
Results are saved in a Redis DB.
The keys are contructed based on the hash (sha1) of the url asked and the id of the ad, separated by a "/".
The value is a JSon of the ad summary.

The key with only the sha1 of the url contains the latest execution of the program for this url.