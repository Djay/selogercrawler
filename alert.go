package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	mailgun "github.com/mailgun/mailgun-go"
)

/**
*  Alert the user when a new ad has been added.
*  The alert can be by email or SMS (or both)
**/

var (
	adSavedToAlert ad
)

// AlertUser sends a notification through the desired channel (should be an enum)
func AlertUser(channels []string, adSaved ad) {
	adSavedToAlert = adSaved
	for _, channel := range channels {
		switch channel {
		case "SMS":
			// Send the alert by SMS through
			sendAlertWithTwilio()
		case "EMAIL":
			// Send the aler by email through mailgun
			if EmailForAlert != "gerald.colin@goovy.com" {
				sendAlertWithMailGun()
			}
		}
	}

}

func sendAlertWithMailGun() {
	domain, apiKey, publicApiKey := GetMailgunCredsFromConf()
	mg := mailgun.NewMailgun(domain, apiKey, publicApiKey)
	m := mg.NewMessage(
		"Alerte SeLoger <donotreply@example.com>",
		"Nouvelle annonce ("+adSavedToAlert.Price+"): "+adSavedToAlert.Title,
		"Nouvelle annonce:\n"+adSavedToAlert.Title+"\n"+adSavedToAlert.ImgLink+"\n"+adSavedToAlert.Href+"\n",
		EmailForAlert,
	)
	// m.AddBCC("bar@example.com")
	m.SetHtml("<html>Nouvelle annonce:<br/><a href=\"" + adSavedToAlert.Href + "\">" + adSavedToAlert.Title + "</a></html>")
	_, id, err := mg.Send(m)
	Check(err)
	fmt.Printf("\nEmail sent to %s with id %s\n", EmailForAlert, id)

}

func sendAlertWithTwilio() {
	fmt.Printf("\n>>> Send alert by SMS with Twilio APIs for %s\n", adSavedToAlert.Title)
	accountSid, authToken := GetTwilioCredsFromConf()
	urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + accountSid + "/Messages.json"

	msgData := url.Values{}
	msgData.Set("To", "+33667662344")
	msgData.Set("From", "+33644645094")
	msgData.Set("Body", adSavedToAlert.Title)
	msgDataReader := *strings.NewReader(msgData.Encode())

	client := &http.Client{}
	req, _ := http.NewRequest("POST", urlStr, &msgDataReader)
	req.SetBasicAuth(accountSid, authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	fmt.Printf("\nSend Twilio request %s\n%s\n", req.Body, req.Header)
	resp, _ := client.Do(req)
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		var data map[string]interface{}
		decoder := json.NewDecoder(resp.Body)
		err := decoder.Decode(&data)
		if err == nil {
			fmt.Println(data["sid"])
		}
	} else {
		fmt.Printf("Error while sending the SMS %s\n%s", resp.Status, resp.Body)
	}
}
