package main

import (
	"bufio"
	"os"
	"strings"
)

// GetRedisCredsFromConf read the config file to get the credentials
func GetRedisCredsFromConf() (addr string, pwd string) {
	file, err := os.Open(CredFilePath + "creds.conf")
	Check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		splitted := strings.Split(scanner.Text(), "=")
		if len(splitted) == 2 {
			switch splitted[0] {
			case "redis_addr":
				addr = splitted[1]
			case "redis_pwd":
				pwd = splitted[1]
			}
		}
	}
	Check(scanner.Err())
	return
}

// GetTwilioCredsFromConf read the config file to get the credentials
func GetTwilioCredsFromConf() (accountSid string, authToken string) {
	file, err := os.Open(CredFilePath + "creds.conf")
	Check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		splitted := strings.Split(scanner.Text(), "=")
		if len(splitted) == 2 {
			switch splitted[0] {
			case "twilio_accountSid":
				accountSid = splitted[1]
			case "twilio_authToken":
				authToken = splitted[1]
			}
		}
	}
	Check(scanner.Err())
	return
}

// GetMailgunCredsFromConf read the config file to get the credentials
func GetMailgunCredsFromConf() (domain string, apiKey string, pubKey string) {
	file, err := os.Open(CredFilePath + "creds.conf")
	Check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		splitted := strings.Split(scanner.Text(), "=")
		if len(splitted) == 2 {
			switch splitted[0] {
			case "mailgun_domain":
				domain = splitted[1]
			case "mailgun_apiKey":
				apiKey = splitted[1]
			case "mailgun_pubKey":
				pubKey = splitted[1]
			}
		}
	}
	Check(scanner.Err())
	return
}
